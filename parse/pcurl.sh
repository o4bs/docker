#!/bin/bash

curl -X $1 \
	-H "X-Parse-Application-Id: 123" \
	-H "Host: parse.com" \
	--url http://dev.o4bs.com:6000/$2
