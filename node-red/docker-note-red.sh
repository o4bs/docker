
#!bin/bash

docker rm node-red
docker run -d --name node-red -v /docker/node-red/run-node-red.sh:/run-node-red.sh -p 1880:1880 o4bs/node-red /run-node-red.sh
