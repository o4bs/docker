#!/bin/bash

# clean all running and exited containers.
docker stop `docker ps -q`
docker rm `docker ps -aq`

# kong-database from postgresql
docker run -d --name kong-database -v /data/postgresql:/var/lib/postgresql/data -p 5432:5432 -e POSTGRES_USER=kong -e POSTGRES_DB=kong postgres:9.4

# kong from mashape
docker run -d --name kong -e DATABASE=postgres --link kong-database:kong-database -p 6000:8000 -p 6443:8443 -p 6001:8001 -p 7946:7946 -p 7946:7946/udp mashape/kong

# kong-dashboard from pgbi
docker run -d --name kong-dashboard -p 8080:8080 pgbi/kong-dashboard

# mongo from mongo
docker run -d --name mongo -v /data/mongodb:/data/db mongo sh -c "mongod"

# rockmongo from webts
docker run -d -p 9090:80 --link mongo:db --name rockmongo webts/rockmongo

# parse from o4bs
docker run -d --name parse --link mongo:mongo -v /docker/parse/run-parse.sh:/run-parse.sh -p 6337:1337 o4bs/parse /run-parse.sh

# mysql from mysql
docker run -d --name mysql -v /data/mysql:/var/lib/mysql -v /docker/mysql/run-mysqld.sh:/run-mysqld.sh -e "MYSQL_ROOT_PASSWORD=Passw0rd" mysql /run-mysqld.sh

# seafile from o4bs
docker run -d --name seafile --link mysql:mysql -v /data/seafile:/seafile -p 9082:9082 -p 9000:8000 o4bs/seafile /seafile/run-seafile.sh
# curl http://dev.o4bs.com:9000/api2/ping/

# redis from redis
docker run --name redis -d -v /data/redis:/data redis redis-server --appendonly yes
# docker run -it --link redis:redis redis redis-cli -h redis -p 6379

# mqtt from o4bs
docker run -d --name mqtt -p 7883:1883 -p 7001:9001 o4bs/mqtt  mosquitto -c /etc/mosquitto/mosquitto.conf

# msg worker/receiver from o4bs
docker run -d --name worker --link redis:redis --link mqtt:mqtt -v /data/msg:/data/msg o4bs/msg  /usr/local/bin/sys_user_mq_worker.py 2>&1 /data/msg/worker.log
docker run -d --name receiver --link redis:redis --link mqtt:mqtt -v /data/msg:/data/msg o4bs/msg  /usr/local/bin/sys_user_mq_receiver.py 2>&1 /data/msg/receiver.log

# apache from o4bs
docker run -d --name apache --link redis:redis --link mysql:mysql --link kong-database:postgres -v /data/apache/www:/var/www -p 8888:8080 o4bs/apache apache2ctl -k start -DFOREGROUND

# tt-node from o4bs
docker run -d --name tt-node --link mongo:mongo --link mqtt:mqtt -v /data/apps/tt-node:/app -p 7337:1337 o4bs/nvm  /app/run-tt-node.sh

